# MAC0110 - MiniEP7
# Lucas Francesco - 11319621

quaseigual(x,y) = abs(x-y) <= 0.001

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
    A[m + 1] = 1 // (m + 1)
    for j = m : -1 : 1
    A[j] = j * (A[j] - A[j + 1])
    end
    end
    return abs(A[1])
end

check_sin(value,x) = quaseigual(value, sin(x))
check_cos(value,x) = quaseigual(value, cos(x))
check_tan(value,x) = quaseigual(value, tan(x))

#since all trig functions are regular and repeats itself, we can limit it from 0 to tau (2pi, a full circle)
function taylor_sin(x)
    x = x%(2*pi)
    n = 0.0
    for i in 0:9
        n += (((-1)^i)*x^(2*i+1))/(factorial(big(2*i+1)))
    end
    n
end

function taylor_cos(x)
    x = x%(2*pi)
    n = 0.0
    for i in 0:9
        n += (((-1)^i)*x^(2*i))/(factorial(big(2*i)))
        ## i1 = ((-1)^0*x^(2*0)) -> 1*1/1 -> 1
    end
    n
end

function taylor_tan(x)
    #tan doesnt need to be 2pi to have its period repeated, we can just use from 0 to pi and have it all covered
    #(pi*1.0) is concise syntax to cast pi to float else we will have a cast/demotion error
    x = x % (pi*1.0)
    #those () at the ternary branches are for julia parser errors https://github.com/JuliaLang/julia/issues/9180
    x == 0 ? (return 0) : x #else we get a NaN because of 0/0
    #tan actually needs error checking for mod pi/2 values because it gets infinite, since all multiples of pi values have returned 0, we can check this safely
    #print(x," - " ,  (x % (pi/2)), "\n")
    x % (pi/2) == 0 ? (return Inf) : x
    i = 0.0
    for n in 0:19
        i += (2^(2*n)*((2^(2*n)-1)*bernoulli(n)*x^(2*n-1)))/(factorial(big(2*n)))
    end
    i
end

function test()
    #a-> sin b -> cos c->tan
    #all have the same size just to save time and not do 3 loops, errors on the first one and just panics
    a = [[-0.5,-π/6],[-0.866,-π/3],[-0.707,-π/4],[0.5,25π/6],[0.866,13π/3],[0,4π]]
    b = [[0.866,-π/6],[0.5,-π/3],[0.707,-π/4],[0.866,25π/6],[0.5,13π/3],[1,4π]]
    c = [[-0.577,-π/6],[-1.732,-π/3],[-1,-π/4],[0.577,25π/6],[1.732,13π/3],[0,4π]]
    for n in 1:length(a)
        check_sin(a[n][1],a[n][2]) ? a : error("wrong sin for (", a[n][2],") (", n," value on index)\n")
        check_cos(b[n][1],b[n][2]) ? b : error("wrong cos for (", b[n][2],") (", n," value on index)\n")
        check_tan(c[n][1],c[n][2]) ? c : error("wrong tan for (", c[n][2],") (", n," value on index)\n")
    end 
end

test()
